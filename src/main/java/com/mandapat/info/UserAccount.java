package com.mandapat.info;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "taccount")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccount {
    @Column(name = "first_name")
    private String f_name;

    @Column(name = "last_name")
    private String l_name;


    @Column(name = "p_word")
    private String password;
    @Id
    @Column(name = "e_mail")
    private String email;

}
