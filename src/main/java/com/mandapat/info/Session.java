package com.mandapat.info;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tsession")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Session {
    @Column(name = "e_mail")
    private String email;

    @Id
    private String uuid;

    @Column(name = "time_stamp")
    private String timestamp;
}
