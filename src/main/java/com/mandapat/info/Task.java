package com.mandapat.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "tlist")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    @Column(name = "e_mail")
    private String email;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long task_id;
    private String task_description;
    private String task_date;
}
