package com.mandapat.setup;

import com.mandapat.dao.*;
import com.mandapat.info.*;
import com.mandapat.server.ServerResponse;
import com.mandapat.views.*;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.views.View;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import static javax.management.timer.Timer.ONE_DAY;

@Path("/trackit")
@Produces(MediaType.TEXT_HTML)
public class TResource {
    private UserPanelView userPanelView;
    private LogoutView logoutView;
    private UserAccountDAO userAccountDAO;
    private TaskDAO taskDAO;
    private SessionDAO sessionDAO;

    public TResource(UserAccountDAO userAccountDAO,TaskDAO taskDAO,SessionDAO sessionDAO) {
       this.userAccountDAO = userAccountDAO;
       this.taskDAO = taskDAO;
       this.sessionDAO = sessionDAO;

    }

    @GET
    @Path("/login")
    public LoginView loginView() {return new LoginView();}

    @GET
    @Path("/create-account")
    public AccountView accountPage() {
        return new AccountView();
    }

    @GET
    @Path("/userpanel")
    @UnitOfWork
    public View userPanelView(@CookieParam("userSession") String sessionCookie) {
        if(sessionCookie != null && !sessionCookie.isEmpty()) {
            Session session = sessionDAO.findSessionByUUID(sessionCookie);
            UserAccount userAccount = userAccountDAO.findUserByEmail(session.getEmail());
            userPanelView = new UserPanelView();
            userPanelView.setFirstName(userAccount.getF_name());
            userPanelView.setUserTask(taskDAO.userTaskList(userAccount.getEmail()));
            userPanelView.setTotalTasks(taskDAO.getTotalTasks(userAccount.getEmail()));

            return userPanelView;
        } else {
            return loginView();
        }



    }

    @GET
    @Path("/logout")
    @UnitOfWork
    public View logoutView(@CookieParam("userSession") String sessionCookie) {
        if(sessionCookie != null && !sessionCookie.isEmpty()) {
            return new LogoutView();
        } else {
            return loginView();
        }

    }

    @POST
    @Path("/userpanel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response createTask(@CookieParam("userSession") String sessionCookie, Task task) {
        Session session = sessionDAO.findSessionByUUID(sessionCookie);
        UserAccount account = userAccountDAO.findUserByEmail(session.getEmail());
        taskDAO.createTask(account.getEmail(),task.getTask_description());
        return Response.ok().build();
    }
    @DELETE
    @Path("/userpanel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response deleteTask(Task task) {
        taskDAO.deleteTask(task.getTask_description());
        return Response.ok().build();
    }

    @POST
    @Path("/create-account")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response createLogin(UserAccount userAccount) {
        ServerResponse serverResponse = new ServerResponse();
        if (userAccountDAO.createUserLogin(userAccount.getF_name(),userAccount.getL_name(),userAccount.getPassword(),userAccount.getEmail())) {
            return Response.ok().build();
        } else {
            serverResponse.setMessage("This account already exists!");
            return Response.ok(serverResponse.getMessage()).build();
        }

    }

    @DELETE
    @Path("/logout")
    @UnitOfWork
    public Response logoutUser(@CookieParam("userSession")String cookie) {
        Session session = sessionDAO.findSessionByUUID(cookie);
        sessionDAO.logoutSession(session.getUuid());
        return Response.ok().build();
    }

    @POST
    @Path("/login")
    @Produces(MediaType.TEXT_HTML)
    @UnitOfWork
    public Response checkLogin(UserAccount userAccount) {
        ServerResponse serverResponse = new ServerResponse();
        Optional<UserAccount> userAccountOptional = Optional.ofNullable(userAccountDAO.authenticateUser(userAccount.getEmail(),userAccount.getPassword()));
        if(userAccountOptional.isPresent()) {
            userAccount = userAccountOptional.get();
            Optional sessionOptional = sessionDAO.createSession(userAccount.getEmail());

            if(!sessionOptional.isPresent()) {

                serverResponse.setMessage("Session not created");
                return Response.ok(serverResponse.getMessage()).build();
            } else {
                Session session = (Session) sessionOptional.get();
                return Response.ok().cookie(
                        new NewCookie(
                                "userSession",
                                session.getUuid(),
                                "/",
                                null,
                                1,
                                "",
                                (int) ONE_DAY,
                                Date.from(Instant.now().plusSeconds(ONE_DAY)),
                                false,
                                false
                        )
                )
                .build();
            }
        } else {
            serverResponse.setMessage("404");
            return Response.ok(serverResponse.getMessage()).build();
        }

    }
}
