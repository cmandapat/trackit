package com.mandapat;

import com.mandapat.dao.SessionDAO;
import com.mandapat.dao.TaskDAO;
import com.mandapat.dao.UserAccountDAO;
import com.mandapat.setup.TConfiguration;
import com.mandapat.info.*;
import com.mandapat.setup.TResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;


public class TApplication extends Application<TConfiguration> {

    public static void main(String[] args) throws Exception{
        new TApplication().run(args);
    }

    private final HibernateBundle<TConfiguration> hibernateBundle = new HibernateBundle<TConfiguration>(UserAccount.class,Task.class,Session.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(TConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public void initialize(Bootstrap<TConfiguration> bootstrap) {
        //add this section later
        bootstrap.addBundle(new ViewBundle());
        bootstrap.addBundle(new AssetsBundle("/assets","/assets"));
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(TConfiguration configuration, Environment environment) {
        //nothing to setup here yet
        final UserAccountDAO userAccountDAO = new UserAccountDAO(hibernateBundle.getSessionFactory());
        final TaskDAO taskDAO = new TaskDAO(hibernateBundle.getSessionFactory());
        final SessionDAO sessionDAO = new SessionDAO(hibernateBundle.getSessionFactory());
        final TResource resource = new TResource(userAccountDAO,taskDAO,sessionDAO);
        environment.jersey().register(resource);
    }
}
