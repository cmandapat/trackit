package com.mandapat.views;

import com.mandapat.info.Task;
import io.dropwizard.views.View;

import java.util.List;

public class UserPanelView extends View {
    private String firstName;
    private List<Task> userTask;
    private List<String> userTaskList;
    private int totalTasks;
    public UserPanelView() {
        super("/assets/userpanelview.mustache");
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setTotalTasks(int totalTask) {this.totalTasks = totalTask;}
    public void setUserTaskList(List<Task> userTask) {
        for (Task task : userTask) {
            userTaskList.add(task.getTask_description());
        }
    }
    public void setUserTask(List<Task> task) {
        this.userTask = task;
    }
    public List<String> getUserTaskList() {return userTaskList;}

    public String getFirstName() {
        return firstName;
    }

    public List<Task> getUserTask() {
        return userTask;
    }
    public int getTotalTasks() {return totalTasks;}

}
