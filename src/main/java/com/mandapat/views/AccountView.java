package com.mandapat.views;

import io.dropwizard.views.View;

public class AccountView extends View {
    public AccountView() {
        super("/assets/accountview.mustache");
    }
}
