package com.mandapat.views;

import io.dropwizard.views.View;

public class RestrictedView extends View {
    public RestrictedView() {super("/assets/restrictedView.mustache");}
}
