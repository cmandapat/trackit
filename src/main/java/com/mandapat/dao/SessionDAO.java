package com.mandapat.dao;

import com.mandapat.info.Session;
import io.dropwizard.hibernate.AbstractDAO;
import lombok.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class SessionDAO extends AbstractDAO<Session> {
    private String INSERT_QUERY = "INSERT into tsession (e_mail,uuid,time_stamp) VALUES (:email,:uuid,:timestamp);";
    private String SELECT_QUERY = "SELECT * from tsession where uuid = :uuid";
    private String DELETE_QUERY = "DELETE from tsession where uuid = :uuid";
    public SessionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Session> createSession(@NonNull String email) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        Date createDate = new Date();
        new Timestamp(createDate.getTime());
        String date = sdf.format(createDate);

        UUID randomUUID = UUID.randomUUID();
        String uuid = randomUUID.toString();
        Session session = Session.builder()
                .email(email)
                .uuid(uuid)
                .timestamp(date)
                .build();

        Query query = currentSession().createNativeQuery(INSERT_QUERY);
        Optional<Session> sessionOptional = Optional.of(session);
        try {
            query.setParameter("email",session.getEmail());
            query.setParameter("uuid", session.getUuid());
            query.setParameter("timestamp",session.getTimestamp());
            query.executeUpdate();
            return sessionOptional;

        } catch (Exception e) {
            System.out.println("Cannot create session for user");
            return Optional.empty();
        }

    }

    public Session findSessionByUUID(@NonNull String uuid) {
        Query query = currentSession().createNativeQuery(SELECT_QUERY,Session.class);
        query.setParameter("uuid",uuid);
        List<Session> session = query.getResultList();
        if(session == null || session.isEmpty()) {
            return null;
        }
        return session.get(0);
    }

    public void logoutSession(@NonNull String uuid) {
        Query query = currentSession().createNativeQuery(DELETE_QUERY,Session.class);
        query.setParameter("uuid",uuid);
        query.executeUpdate();
    }

}
