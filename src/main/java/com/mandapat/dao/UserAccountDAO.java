package com.mandapat.dao;
import com.mandapat.info.UserAccount;
import io.dropwizard.hibernate.AbstractDAO;
import lombok.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.SQLException;
import java.util.List;

public class UserAccountDAO  extends AbstractDAO<UserAccount> {
    private final String CREATE_ACCOUNT_QUERY = "INSERT INTO taccount (first_name,last_name,p_word,e_mail) VALUES (:f_name,:l_name,:password,:email);";
    private final String SELECT_QUERY = "SELECT * FROM taccount WHERE e_mail = :email";


    public UserAccountDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public UserAccount findUserByEmail(@NonNull String email) {
        Query query = currentSession().createNativeQuery(SELECT_QUERY,UserAccount.class);
        query.setParameter("email",email);
        List<UserAccount> userAccount = query.getResultList();
        if(userAccount == null || userAccount.isEmpty()) {
            return null;
        }

        return userAccount.get(0);
    }

    public UserAccount authenticateUser(@NonNull String email, @NonNull String password) {
        Query query = currentSession().createNativeQuery(SELECT_QUERY,UserAccount.class);
        query.setParameter("email",email);
        List<UserAccount> userAcccountList = query.getResultList();
        UserAccount userAccount = userAcccountList.get(0);

        if (BCrypt.checkpw(password,userAccount.getPassword()) && userAcccountList.size() != 0) {
            return userAccount;
        } else {
            return null;
        }
    }


    public Boolean createUserLogin(@NonNull String firstName, @NonNull String lastName, @NonNull String password, @NonNull String email) {
        UserAccount userAccount = UserAccount.builder()
                .f_name(firstName)
                .l_name(lastName)
                .password(password)
                .email(email)
                .build();

        Query query = currentSession().createNativeQuery(CREATE_ACCOUNT_QUERY);

        try {
            query.setParameter("f_name",userAccount.getF_name());
            query.setParameter("l_name",userAccount.getL_name());
            query.setParameter("password", BCrypt.hashpw(userAccount.getPassword(),BCrypt.gensalt()));
            query.setParameter("email",userAccount.getEmail());
            return query.executeUpdate() == 1;

        }catch (Exception e) {
            return false;
        }

    }

}
