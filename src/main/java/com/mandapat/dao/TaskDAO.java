package com.mandapat.dao;

import com.mandapat.info.Task;
import io.dropwizard.hibernate.AbstractDAO;
import lombok.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Calendar;

public class TaskDAO extends AbstractDAO<Task> {

    private final String CREATE_QUERY = "INSERT INTO tlist (e_mail,task_description,task_date) VALUES (:email,:task_description,:task_date);";
    private final String SELECT_QUERY = "SELECT * FROM tlist WHERE e_mail = :email";
    private final String DELETE_TASK_QUERY = "DELETE FROM tlist WHERE task_description = :task_description";
    public TaskDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Boolean createTask(@NonNull String email, @NonNull String task) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = formatter.format(date);
        Task userTask = Task.builder()
                .email(email)
                .task_description(task)
                .task_date(strDate)
                .build();

        Query query = currentSession().createNativeQuery(CREATE_QUERY);
        try {
            query.setParameter("email",email);
            query.setParameter("task_description",userTask.getTask_description());
            query.setParameter("task_date",userTask.getTask_date());
            return query.executeUpdate() == 1;
        } catch(Exception e) {
            return false;
        }
    }

    public List<Task> userTaskList(@NonNull String email) {
        Query query = currentSession().createNativeQuery(SELECT_QUERY,Task.class);
        query.setParameter("email",email);
        List<Task> userTask = query.getResultList();
        if(userTask == null || userTask.isEmpty()) {
            return null;
        }
        
        return userTask;

    }

    public int getTotalTasks(@NonNull String email) {
        Query query = currentSession().createNativeQuery(SELECT_QUERY,Task.class);
        query.setParameter("email",email);
        List<Task> userTask = query.getResultList();
        if(userTask == null || userTask.isEmpty()) return 0;
        return userTask.size();
    }
    public void deleteTask(@NonNull String task_description) {
        Query query = currentSession().createNativeQuery(DELETE_TASK_QUERY,Task.class);
        query.setParameter("task_description",task_description);
        query.executeUpdate();

    }

}
