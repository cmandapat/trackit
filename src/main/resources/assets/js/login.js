$(document).ready(function(){
    function loginFunction(){
        var emailEntered = $("#email").val()
        var passwordEntered = $("#password").val()

        if(emailEntered != '' && passwordEntered != '') {
            $.ajax({
                type:"POST",
                url:"/trackit/login",
                headers: {
                    'Accept': 'text/html',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    email: emailEntered,
                    password: passwordEntered
                }),
                success:function(response) {
                    //window.location = "/trackit/userpanel";
                    if(response.toString() == "404") {
                        document.getElementById('email').value = '';
                        document.getElementById('password').value = '';
                        document.getElementById('loginWarning').style.display = "block";
                    } else {
                        window.location = "/trackit/userpanel"
                    }
                }
            });
        } else {
            document.getElementById('email').value = '';
            document.getElementById('password').value = '';
            document.getElementById('validWarning').style.display = "block";
        } 
    }
    document.getElementById('password').onkeydown = function(event) {
        if(event.keyCode == 13) {
            loginFunction();
        }
    }
    document.getElementById('email').onkeydown = function(event) {
        if(event.keyCode == 13) {
            loginFunction();
        }
    }
    document.getElementById('login').onclick = function() {
        loginFunction();
    }    
 }); 

