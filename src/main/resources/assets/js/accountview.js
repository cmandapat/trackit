$(document).ready(function() {
    $("#create-account").click(function() {
        var firstnameEntered = $("#f_name").val()
        var lastnameEntered = $("#l_name").val()
        var passwordEntered = $("#password").val()
        var emailEntered = $("#email").val()
        var password2Entered = $("#password2").val()
        var passwordLength = passwordEntered.length;
        const passwordMin = 6;

        var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
        if(emailFilter.test(emailEntered) && passwordEntered == password2Entered && passwordEntered != "" && 
        password2Entered != "" && firstnameEntered != "" && lastnameEntered != "" && passwordLength > passwordMin) {
            $.ajax({
                type:"POST",
                url: "/trackit/create-account",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    f_name: firstnameEntered,
                    l_name: lastnameEntered,
                    password: passwordEntered,
                    email: emailEntered
                }),
                success: function(response) {
                    window.location = "/trackit/login";
                },
                error: function() {
                     document.getElementById('f_name').value = '';
                     document.getElementById('l_name').value = '';
                     document.getElementById('password').value = '';
                     document.getElementById('email').value = '';
                     document.getElementById("emailExist").style.display = "block";
                }
            });
        } else if (!emailFilter.test(emailEntered)){
            document.getElementById('f_name').value = '';
            document.getElementById('l_name').value = '';
            document.getElementById('password').value = '';
            document.getElementById('email').value = '';
            document.getElementById("emailWarning").style.display = "block";
        } else if(passwordEntered == "") {
            document.getElementById("passwordWarning").style.display = "block";
        } else if (password2Entered == "") {
            document.getElementById("passWord2Warning").style.display = "block";
        } else if (firstnameEntered == "") {
            document.getElementById("fNameWarning").style.display = "block";
        } else if(lastnameEntered == "") {
            document.getElementById("lNameWarning").style.display = "block";
        } else if (passwordLength < passwordMin) {
            document.getElementById('password').value = '';
            document.getElementById('password2').value = '';
            document.getElementById("shortPassword").style.display = "block";
        }
         else {
            document.getElementById('f_name').value = '';
            document.getElementById('l_name').value = '';
            document.getElementById('password').value = '';
            document.getElementById('email').value = '';
            alert("Please fill in all slots");
        }

    });
});