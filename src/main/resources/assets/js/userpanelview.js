function deleteTask(e) {
    var taskDesc = e.currentTarget.parentNode.querySelector('.task_descr').innerHTML;
    $.ajax({
        type:"DELETE",
        url: "/trackit/userpanel",
        headers: {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
        data: JSON.stringify({
            task_description: taskDesc
        }),
        success: function() {
            window.location = window.location;
        }
    })
}

$(document).ready(function() {
    if ( $('#goals-container').children().length > 0 ) {
        $('#goals-list').slideDown();
    }

    document.getElementById('task_description').onkeydown = function(event) {
        if(event.keyCode == 13) {
            var taskdescriptionEntered = $("#task_description").val();
                $.ajax({
                    type: "POST",
                    url: "/trackit/userpanel",
                    headers: {
                        'Accept' : 'application/json',
                        'Content-Type' : 'application/json'
                    },
                data: JSON.stringify({
                    task_description: taskdescriptionEntered
                }),
                success: function() {
                    location.reload();
                }
                });
            }
    }    
});