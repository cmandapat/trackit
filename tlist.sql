create table tlist (
    e_mail VARCHAR(255) not null,
    task_id int not null primary key auto_increment,
    task_description varchar(255) not null,
    task_date varchar(255) not null
);